# README #


### What is this repository for? ###

This is a custom module for Home Assistant developed to be used together with deConz and a RaspBee.

### How do I get set up? ###

- Copy the contents of `custom_components` to your own `custom_components`
- Add following to `configuration.yaml` :
~~~~
dcz:
  ws_url: ws://your.ip.of.deConz:443
  api_key: yourAPIKey
  host: your.ip.of.deConz
~~~~

Follow this guide how to retrieve a API Key : http://dresden-elektronik.github.io/deconz-rest-doc/getting_started/ 
Read **Acquire an API key** and follow the guide there.

My system runs Home Assistant 0.50.1

### What works at the moment ###
These are the devices that i have in my home which i use every day :

- IKEA bulbs of type E27, E14 and GU10
- IKEA 4 button remote control
- IKEA Motion Sensor
- Philips Hue Dim Switch
- Philips Motion Sensor
- Osram LightPole
- Xiaomi Cube
- Xiaomi Aqara Door Sensor (thx to @romanski)

### How does it work ###
When Home Assistant is started it calls the module then the module calls the REST API from deConz and gets the `lights` and `sensors` and adds these to HASS in following way :

- Lights are added as a `light`
- Sensors of type presense aka motion are added as `binary_sensors`
- deConz groups are added as `light`
- Battery information from sensors are added as a `sensor`
- Temperature from Philips Motion Sensor is added as a `sensor`
- LightLevel reading from Philips Motion Sensor is added as a `sensor`
- All changes on a switch in deConz will end in HASS as an event so if you need to act for example on pushing a button on the IKEA remote you need to add an automation in `automation.yaml` that looks like this :
~~~~
  - alias: "Toggle Stue gang"
    initial_state: true
    trigger:
      platform: event
      event_type: dcz_event
      event_data:
        id: My button in livingroom
        button: 1002
    action:
      - service: light.toggle
        entity_id: light.mainroom
~~~~

Notice the `event_type: dcz_event` and `id` is the tag `name` that is given to the deConz REST API sensor resource which can be found by using POST MAN   : https://www.getpostman.com/postman and by doing a `GET` on the URL http://your.ip.of.deConz:80/api/yourAPIKey/sensors

A great help to debug and find out if the events from deConz are coming is using **Simple WebSocke Client** within Chrome, please find it here : https://chrome.google.com/webstore/detail/simple-websocket-client/pfdhoblngboilpfeibdedpjgfnlcodoo?hl=en

**All generated entity names in HASS can be found in States in HASS web interface.**

### Known issues to be resolved ###
NOTE: The code is **very childish** and it's not up to the standard that i'd like to deliver but upon request i share it here. It was done very quick to get something started and i haven't had time to improve it. 

+ Here are what needs to be improved :
  * I need to redo the way i find what kind of event it is done in `async_process_message` is too simple and causes problems when needing to add new things.
  * If you restart deConz then the websockets events from deConz are not received anymore since i don't retry to restablish the websocket again.
  * The way i convert colors for OSRAM has some problems and is wrong however it sort of works for now at least until i find time to rewrite the code.
  * If you rename a device in deConz you need to restart HASS to get the new names
  * New devices added in deConz requires restart of HASS
  * I don't use the `reachable` from deConz in HASS to hide/unhide devices at goes away
  * Groups in HASS can get unsyncronized when lights are part of multiple groups and you switch on/off group.
  * I have no idea how the module behaves if other type of devices i don't know of in deConz are added.

### Who do I talk to? ###

* @mihaicph
* Join the discord room for discussion https://discord.gg/TRJy2Yp